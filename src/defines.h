#define MAX_DIST INT_MAX
#define MAX_CANDIDATES 30
#define RHO 0.5
#define N_ITERS 11
#define DELTA 0.001
#define N_STATES 3

#define N_POINTS 2600
#define N_FEATURES 5
#define K_NEIGHBORS 50
#define L2_K_NEIGHBORS 6 // l2(K_NEIGHBORS+1), rounded up

#define N_LEAF 690
#define LEAF_SIZE 50

#define RP_TREE_INIT 1